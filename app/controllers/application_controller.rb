
#Листинг 8.11: 


class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  private
#Листинг 11.31: Перемещение метода logged_in_user в контроллер Application. app/#controllers/application_controller.rb

    # Проверяет статус входа пользователя.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end

