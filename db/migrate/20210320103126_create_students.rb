class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :country
      t.string :fio
      t.string :lastname
      t.string :firstname
      t.string :email
      t.string :comment
      t.integer :user_id
 t.references :user, index: true, foreign_key: true
      
 t.timestamps null: false
    end
 add_index :students, [:user_id, :created_at]
  end
end
